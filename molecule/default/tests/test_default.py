import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_bitbucket_proxy_index(host):
    # This tests that traefik forwards traffic to the conda-bot web server
    # and that the web server is online
    cmd = host.command('curl -H Host:ics-ans-role-bitbucket-proxy-default -k -L https://localhost')
    assert 'OK' in cmd.stdout
