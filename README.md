# ics-ans-role-bitbucket-proxy

Ansible role to install bitbucket-proxy.

## Role Variables

```yaml
bitbucket_proxy_image: registry.esss.lu.se/ics-infrastructure/bitbucket-proxy
bitbucket_proxy_tag: latest
bitbucket_proxy_frontend_rule: "Host:{{ ansible_fqdn }}"
bitbucket_proxy_gl_trigger_token: "xxxxxx"
bitbucket_proxy_sentry_dsn: ""
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-bitbucket-proxy
```

## License

BSD 2-clause
